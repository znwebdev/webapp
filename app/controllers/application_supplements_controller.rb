class ApplicationSupplementsController < ApplicationController
  def show
    @supplement = ApplicationSupplement.find params[:id]
    if @supplement.document.blank?
      redirect_to @supplement.application
    else
      redirect_to @supplement.document.url
    end
  end

  def update
    @supplement = ApplicationSupplement.find params[:id]

    if @supplement.update(supplement_params)
      @supplement.send_for_review
      redirect_to application_url(@supplement.application_id)
    else
      redirect_to Application.find params[:application_id]
    end
  end

  def approve
    @supplement = ApplicationSupplement.find params[:id]
    if @supplement.approve
      flash.notice = "#{@supplement.title} approved."
    else
      flash.notice = "Error."
    end
    redirect_to @supplement.application
  end

  def deny
    @supplement = ApplicationSupplement.find params[:id]
    if @supplement.deny
      flash.notice = "#{@supplement.title} denied."
      NotificationMailer.document_denied(@supplement).deliver_now
      @supplement.application.deny
      NotificationMailer.application_denied(@supplement.application).deliver_now
      @supplement.application.card.destroy if @supplement.application.card
    else
      flash.notice = "Error."
    end
    redirect_to @supplement.application
  end

  private

  def supplement_params
    params.require(:application_supplement).permit(:document)
  end
end
