class ApplicationsController < ApplicationController
  before_action :find_app, only: [:show, :edit, :update]

  def index
    @apps = current_user.applications
  end

  def show
  end

  def new
    @app = Application.new
  end

  def create
    @app = current_user.applications.new(app_params)
    if @app.save
      ['Driver License', 'Bank Statement or Utility Bill', 'Recent Photograph'].each do |doc|
        AddSupplement.run(application: @app, title: doc)
      end
      @app.request_documents
      redirect_to application_url(@app)
    else
      render :new
    end
  end

  def edit
  end

  def approve
    @app = Application.find(params[:id])

    if @app.approve
      @card = @app.build_card
      @card.save
      NotificationMailer.application_approved(@app).deliver_now
    end

    redirect_to @app
  end

  def deny
    @app = Application.find(params[:id])
    if @app.deny
      NotificationMailer.application_denied(@app).deliver_now
      @app.card.destroy if @app.card
    end
    redirect_to @app
  end

  private

  def app_params
    params.require(:application).permit(:first_name, :last_name, :email, :address1, :city, :state, :zip, :fee_cents, :title)
  end

  def find_app
    @app = Application.find params[:id]
  end
end
