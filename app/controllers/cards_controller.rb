class CardsController < ApplicationController
  def show
    @app = Application.find params[:id]

    redirect_to @app if @app and @app.card.blank?

    respond_to do |format|
      format.pdf do
        pdf = IdCardPdf.new(@app)
        send_data pdf.render, filename: @app.card.card_number + '.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end
end
