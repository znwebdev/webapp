class ApplicationForm < Reform::Form
  property :first_name
  property :last_name
  property :title
  property :email
  property :address1
  property :address2
  property :city
  property :state
  property :zip
  property :fee_cents
  property :notes

  validates :first_name, presence: true
end