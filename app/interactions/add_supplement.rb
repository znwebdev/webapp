class AddSupplement < ActiveInteraction::Base
  object :application
  string :title

  def execute
    supplement = application.application_supplements.build(title: title)

    unless supplement.save
      errors.add(supplement, 'could not be added')
    end

    supplement
  end
end