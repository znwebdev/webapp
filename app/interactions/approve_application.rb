class ApproveApplication < ActiveInteraction::Base
  string :haiku
  validates :haiku, presence: true

  def execute

    app = Application.find_by_haiku(haiku)
    errors.add(haiku, 'is not a valid haiku') if app.blank?
    return nil if app.blank?
    if app.approved?
      return app
    else
      id_card = app.build_card
      app.approve
      id_card.expiration = Time.zone.now + 1.year
      id_card.save
      NotificationMailer.application_approved(app).deliver_now
    end
    app
  end
end