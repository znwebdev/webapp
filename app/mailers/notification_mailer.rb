class NotificationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.documents_received.subject
  #
  def documents_received(application)
    @application = application

    mail to: @application.user.email, from: 'docs-no-reply@zachapps.com'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.document_denied.subject
  #
  def document_denied(supplement)
    @supplement = supplement

    mail to: @supplement.application.user.email, from: 'docs-no-reply@zachapps.com'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.application_approved.subject
  #
  def application_approved(application)
    @application = application

    attachments['ID CARD ' + @application.token + '.pdf'] = IdCardPdf.new(@application).render

    mail to: @application.user.email, from: 'decisions-no-reply@zachapps.com'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification_mailer.application_denied.subject
  #
  def application_denied(application)
    @application = application

    mail to: @application.user.email, from: 'decisions-no-reply@zachapps.com'
  end

  def admin_doc_email(application)
    @application = application

    mail to: 'nusbaumz@msu.edu', from: 'admin-notifier@zachapps.com'
  end
end
