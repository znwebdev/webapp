class Application < ApplicationRecord
  before_create :set_token
  before_create :set_haiku
  before_create :geocode
  before_create :set_initial_status
  before_save :set_coords

  has_one :card, dependent: :destroy
  belongs_to :user

  has_many :application_supplements, dependent: :destroy

  monetize :fee_cents, allow_nil: true
  geocoded_by :full_street_address
  has_paper_trail only: [:status]

  enum status: [:submitted, :processing, :approved, :denied, :canceled, :deferred, :insufficient, :waiting_for_documents] do

    event :approve do
      before do
        self.approved_at = Time.zone.now
      end
      transition [:submitted, :processing, :deferred, :insufficient, :waiting_for_documents] => :approved
    end

    event :deny do
      before do
        self.approved_at = nil
      end
      transition [:approved, :processing] => :denied
    end

    event :defer do
      transition [:submitted, :processing] => :deferred
    end

    event :mark_insufficient do
      transition [:submitted, :processing] => :insufficient
    end

    event :begin_processing do
      transition all - [:denied] => :processing
    end

    event :request_documents do
      transition :submitted => :waiting_for_documents
    end
  end

  def full_street_address
    if address2.blank?
      "#{address1}, #{city}, #{state}, #{zip}"
    else
      "#{address1}, #{address2}, #{city}, #{state}, #{zip}"
    end
  end

  def regenerate_token
    self.token = ULID.generate
    self.save
  end

  def regenerate_haiku
    self.haiku = Haikunator.haikunate(1000, '-')
    self.save
  end

  def show_title
    self.title.blank? ? "Membership Application (#{token})" : title
  end

  private

  def set_token
    self.token = ULID.generate
  end

  def set_haiku
    self.haiku = Haikunator.haikunate(1000, '-')
  end

  def set_coords
    if self.address1_changed?
      self.geocode
    end
  end

  def set_initial_status
    self.status = 0
  end
end
