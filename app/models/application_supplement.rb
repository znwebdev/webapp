class ApplicationSupplement < ApplicationRecord
  belongs_to :application
  before_create :set_token
  after_update :check_if_all_uploaded

  mount_uploader :document, DocumentUploader
  has_paper_trail only: [:status]

  enum status: [:waiting, :pending_review, :approved, :denied] do
    event :send_for_review do
      transition :waiting => :pending_review
    end

    event :approve do
      transition :pending_review => :approved
    end

    event :deny do
      transition [:pending_review, :waiting] => :denied
    end
  end

  private

  def set_token
    self.token = ULID.generate
  end

  def check_if_all_uploaded
    if self.application.application_supplements.where(status: 0).any?
      return false
    else
      if self.application.status == 'waiting_for_documents'
        self.application.begin_processing
        NotificationMailer.documents_received(self.application).deliver_now
        NotificationMailer.admin_doc_email(self.application).deliver_now
      end
    end
  end
end
