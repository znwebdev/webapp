class Card < ApplicationRecord
  before_create :set_card_number
  before_create :set_expiration
  belongs_to :application

  private

  def set_card_number
    self.card_number = ULID.generate
  end

  def set_expiration
    self.expiration = Time.zone.now + 1.year
  end
end
