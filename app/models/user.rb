class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  has_many :applications

  before_create :set_token
  before_create :set_haiku

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  private

  def set_token
    self.token = ULID.generate
  end

  def set_haiku
    self.haiku = Haikunator.haikunate(1000, '-')
  end
end
