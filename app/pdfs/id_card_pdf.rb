class IdCardPdf < Prawn::Document
  require 'prawn/qrcode'
  def initialize(app)
    super()
    @app = app
    text_content
    supplements
  end

  def supplements
    move_down 20
    formatted_text [
      { text: "SUBMITTED SUPPLEMENTS\n", font_size: 32, styles: [:bold, :underline] }
    ]
    @app.application_supplements.each do |s|
      formatted_text [
                         { text: s.title + " - ", styles: [:bold] },
                         { text: s.token + " ", font: 'Courier'},
                         { text: "(" + s.status + ")", font: 'Courier' }
                     ]
    end
  end


  def text_content
    # stroke_axis
    bounding_box([100,700], width: 300, height: 300) do
      stroke_bounds
      move_down 5
      font('Courier') do
        font_size 18 do
          text Prawn::Text::NBSP * 3 + "ID Card", style: :bold
        end
        text Prawn::Text::NBSP * 3 + @app.card.card_number
        move_down 5
        text Prawn::Text::NBSP * 3 + "Issued: #{@app.card.created_at.strftime('%m/%d/%Y')}"
        text Prawn::Text::NBSP * 3 + "Expires: #{@app.card.expiration.strftime('%m/%d/%Y')}"
        move_down 10
        font_size 16 do
          text Prawn::Text::NBSP * 3 + "#{@app.last_name}, #{@app.first_name}"
        end
        text Prawn::Text::NBSP * 5 + @app.address1
        text Prawn::Text::NBSP * 5 + "#{@app.city}, #{@app.state} #{@app.zip}"
        move_down 10
        text Prawn::Text::NBSP * 3 + "Application ID:"
        text Prawn::Text::NBSP * 5 + @app.token
        move_down 5
        text Prawn::Text::NBSP * 5 + @app.haiku
        text Prawn::Text::NBSP * 3 + "Application Date:"
        text Prawn::Text::NBSP * 5 + @app.created_at.strftime('%m/%d/%Y')
        move_down 12
        font_size 10 do
          move_down 8
          text Prawn::Text::NBSP * 3 + "NOT VALID WITHOUT GOVERNMENT ISSUED PHOTO ID"
        end

        move_down 18
        text Prawn::Text::NBSP * 3 + "X___________________________________"
        text Prawn::Text::NBSP * 5 + "#{@app.last_name}, #{@app.first_name}"
        move_down 5
        text Prawn::Text::NBSP * 18 + "Printed: #{Time.zone.now.strftime('%m/%d/%Y')}"
      end
    end

    move_down 10

    font_size 14 do
      text "PLEASE PRINT AND SAVE THIS MEMBERSHIP CARD."
      text "1) YOU MUST RENEW BEFORE THE EXPIRATION DATE."
      text "2) PLEASE SAFEGUARD YOUR ID. THESE NUMBERS ARE SENSITIVE."

    end

  end

end