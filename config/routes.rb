Rails.application.routes.draw do

  resources :applications do
    member do
      patch :approve, to: 'applications#approve'
      get :deny, to: 'applications#deny'
      get :card, to: 'cards#show'
    end
    resources :application_supplements, only: [:update, :show] do
      member do
        get :approve, to: 'application_supplements#approve'
        get :deny, to: 'application_supplements#deny'
      end
    end
  end

  devise_for :users

  root 'applications#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
