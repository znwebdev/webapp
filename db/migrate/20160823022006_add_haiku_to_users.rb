class AddHaikuToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :haiku, :string
    add_index :users, :haiku, unique: true
  end
end
