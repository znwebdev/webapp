class CreateApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :applications do |t|
      t.string :token
      t.string :haiku
      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.decimal :latitude
      t.decimal :longitude
      t.integer :fee_cents
      t.integer :status
      t.text :notes

      t.timestamps
    end
    add_index :applications, :token, unique: true
    add_index :applications, :haiku, unique: true
  end
end
