class AddTypeToApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :type, :string
  end
end
