class CreateCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cards do |t|
      t.string :card_number
      t.references :application, foreign_key: true
      t.boolean :active, default: true
      t.datetime :expiration

      t.timestamps
    end
    add_index :cards, :card_number, unique: true
  end
end
