class AddApprovedAtToApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :approved_at, :datetime
  end
end
