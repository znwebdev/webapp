class CreateApplicationSupplements < ActiveRecord::Migration[5.0]
  def change
    create_table :application_supplements do |t|
      t.string :token
      t.string :document
      t.references :application, foreign_key: true
      t.boolean :reviewed, default: false

      t.timestamps
    end
    add_index :application_supplements, :token, unique: true
  end
end
