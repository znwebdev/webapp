class AddStatusToDocuments < ActiveRecord::Migration[5.0]
  def change
    add_column :application_supplements, :status, :integer, default: 0
  end
end
