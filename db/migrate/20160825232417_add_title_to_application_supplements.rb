class AddTitleToApplicationSupplements < ActiveRecord::Migration[5.0]
  def change
    add_column :application_supplements, :title, :string
  end
end
