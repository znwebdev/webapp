# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160826040944) do

  create_table "application_supplements", force: :cascade do |t|
    t.string   "token"
    t.string   "document"
    t.integer  "application_id"
    t.boolean  "reviewed",       default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "status",         default: 0
    t.string   "title"
    t.index ["application_id"], name: "index_application_supplements_on_application_id"
    t.index ["token"], name: "index_application_supplements_on_token", unique: true
  end

  create_table "applications", force: :cascade do |t|
    t.string   "token"
    t.string   "haiku"
    t.string   "title"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.integer  "fee_cents"
    t.integer  "status"
    t.text     "notes"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "type"
    t.datetime "approved_at"
    t.integer  "user_id"
    t.index ["haiku"], name: "index_applications_on_haiku", unique: true
    t.index ["token"], name: "index_applications_on_token", unique: true
    t.index ["user_id"], name: "index_applications_on_user_id"
  end

  create_table "cards", force: :cascade do |t|
    t.string   "card_number"
    t.integer  "application_id"
    t.boolean  "active",         default: true
    t.datetime "expiration"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["application_id"], name: "index_cards_on_application_id"
    t.index ["card_number"], name: "index_cards_on_card_number", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "token"
    t.string   "haiku"
    t.boolean  "admin",                  default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["haiku"], name: "index_users_on_haiku", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["token"], name: "index_users_on_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",                         null: false
    t.integer  "item_id",                           null: false
    t.string   "event",                             null: false
    t.string   "whodunnit"
    t.text     "object",         limit: 1073741823
    t.datetime "created_at"
    t.text     "object_changes", limit: 1073741823
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

end
