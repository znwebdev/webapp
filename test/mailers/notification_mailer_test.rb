require 'test_helper'

class NotificationMailerTest < ActionMailer::TestCase
  test "documents_received" do
    mail = NotificationMailer.documents_received
    assert_equal "Documents received", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "document_denied" do
    mail = NotificationMailer.document_denied
    assert_equal "Document denied", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "application_approved" do
    mail = NotificationMailer.application_approved
    assert_equal "Application approved", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "application_denied" do
    mail = NotificationMailer.application_denied
    assert_equal "Application denied", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
