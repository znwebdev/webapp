# Preview all emails at http://localhost:3000/rails/mailers/notification_mailer
class NotificationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/documents_received
  def documents_received
    NotificationMailer.documents_received(Application.last)
  end

  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/document_denied
  def document_denied
    NotificationMailer.document_denied(ApplicationSupplement.last)
  end

  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/application_approved
  def application_approved
    NotificationMailer.application_approved(Application.where(status: 2).last)
  end

  # Preview this email at http://localhost:3000/rails/mailers/notification_mailer/application_denied
  def application_denied
    NotificationMailer.application_denied(Application.last)
  end

end
